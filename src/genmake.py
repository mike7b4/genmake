#!/usr/bin/python
# kate: space-indent on; indent-width 4; replace-tabs on; remove-trailing-spaces all;

import sys
import os
import json
import datetime
import subprocess
from hashlib import md5
from makefile import *
from application import *
from parsefile import *
from readdir import *

VERSION="0.0.2"

class Manager(Application):
    def __init__(self, version):
        Application.__init__(self, "Genmake", version)
        self.file_objects=[]
        self.proj_dir = os.path.abspath(os.curdir)
        self.max_lines=100
        #those added/changed in commandline to be submitted to json file
        self.arg_current_target=""
        #self.arg_targets=[]
        #as soon as the file is saved we kill arg_*
        self.pkgconfigs=[]
        self.targets={}
        self.project={}
        self.toolchains={}
        self.json_file=""
        self.genmakehost="7b4.se"
        self.create_json=False
        self.arg_already_parsed=""
        self.readdir=ReadDir(self)
        self.makefile=Makefile(self)
        self.save = False
        #self.debug = True

    def add_toolchain(self, toolchain, name, overwrite, header):
        if self.validate_toolchain(toolchain, name, overwrite, header)==False:
            return False

        self.toolchains[name]=toolchain
        self.print_verbose("Toolchain: "+name+" added")
        return True

    # we better use a json preloaded template instead of this if/else HELL
    def add_target(self, target, name, overwrite, header):
        self.print_debug("add_target: "+name+header)
        if self.validate_target(target, name, header, overwrite)==False:
            return False

        # passed to TARGET done
        if target.has_key("target_post_rule")==False:
            target["target_post_rule"]=[]

        if target.has_key("include_dirs")==False:
            target["include_dirs"]=[]

        if target.has_key("exclude_dirs")==False:
            target["exclude_dirs"]=[]

        excl = []
        for item in target["exclude_dirs"]:
            if item[0] != os.path.sep:
                excl.append(os.path.abspath(os.path.curdir)+os.path.sep+item)
            else:
                excl.append(os.path.abspath(item))

#        target["exclude_dirs"] = excl

        self.targets[name]=target
        if self.project.has_key("current_target") == False or self.project["current_target"] == "":
            self.project["current_target"] = name

        self.print_verbose("Target: "+name+" added"+str(target))
        return True

    def arg_validate_next(self, arg, argnext, strict=1):
        try:
            argv=argnext.split(":")
            # EXception if None but if it find "-" it will not raise except
            #but continue to below anyway and sys.exit after print error
            args=len(argv)
            if argv[0].find("-")!=0 and (args==strict or (args % 2) == 0):
                self.arg_already_parsed=argnext
                return argv
        except:
            pass

        self.print_error("Commandline error "+arg+" want a <VALUE> or <VALUES> separated by ':' example: genmake.py "+arg+" name:bar:toolchain:arm-elf")
        self.mustdie(-1)

    def check_arg(self, arg, argnext):
        #TODO: make a table of this mess
        if self.arg_already_parsed==arg:
            self.arg_already_parsed=""
            return True
        elif os.path.isdir(arg)==True:
            self.proj_dir=os.path.abspath(arg)
        elif arg=="--create":
            self.create_json=True
        elif arg=="--add-target" or arg=="-A":
            argv=self.arg_validate_next(arg, argnext, 0)
            target={}
            index=0
            print(argv)
            while index<len(argv):
                if argv[index]=="name" or argv[index]=="":
                    name=argv[index+1]
                else:
                    target[argv[index]]=argv[index+1]
                index=index+2

            self.print_debug(target)
            #add_target will exit with an error on fail
            return self.add_target(target, name, True, "Commandline")
        elif arg=="--add-toolchain":
            argv=self.arg_validate_next(arg, argnext, 0)
            toolchain={}
            index=0
            name=""
            #argv is a returned splitted string holding key:value pairs
            #fixme I guess pytho can handle this better thn my crapcode below
            while index<len(argv):
                #the name is used in loookuptable if key is "" we set it as name
                # example --add-toolchain :mynewcooltoolchain or
                # --add-toolchain name:mynewtoolchan is both allowed
                if argv[index]=="name" or argv[index]=="":
                    name=argv[index+1]
                else:
                    toolchain[argv[index]]=argv[index+1]

                index=index+2

            self.print_debug(toolchain)
            #add_toolchain will exit with an error on fail allow overwrite==True
            return self.add_toolchain(toolchain, name, True, "Commandline")
        elif arg=="--set-target" or arg=="-t":
            self.arg_validate_next(arg, argnext)
            #target is actually a key and key is ALWAYS lowercased in our json "db"
            self.arg_current_target=argnext.lower()
            self.print_ok(self.arg_current_target)
        elif arg=="--help":
            print ("    [DIRECTORY]\t\t\tChange directory to specified before we execute genmake commands.\n\t\t\t\tIf this is not specified it defaults to where you execute genmake at the time!")
            print ("    --create\t\t\tcreate json project file if not already exist")
            print ("    --set-target, -t <NAME>\tSet current target, <NAME> is the target name\n\t\t\t\tThis command will recreate Makefile with target options")
            print ("    --add-target, -A [<KEY:VALUE>] Create a new target with key value pairs specified\n\t\t\t\tExample: genmake.py --add-target name:mycooltesttarget:toolchain:arm-elf\n\t\t\t\tname MUST be included the rest is upto u")
            print ("    --add-toolchain [<KEY:VALUE>]\tCreate a new toolchain with key value pairs specified\n\t\t\t\tExample: genmake.py --add-toolchain name:arm-elf:prefix:arm-elf-\n\t\t\t\tname MUST be included the rest is upto u prefix is added to gcc above example means arm-elf-gcc and so on...")
        else:
            self.print_warning( "Uknown commandline argument '"+arg+"'")
            return False

        return True

    #remove
    def lookup(self, lis, value):
        self.print_debug("does list %s hold %s? " % (str(lis), value))
        for v in lis:
            if v==value:
                self.print_verbose("yes")
                return True

            self.print_debug(str(lis)+"===="+(v) )

        self.print_verbose("no")
        return False

    def validate_target(self, target, name, header, overwrite):
        name=name.lower()
        if name=="":
            self.print_error(header+" Target must have a name, I Will abort")
            self.mustdie(-1)
            return False

        if self.targets.has_key(name)==True and overwrite==False:
            self.print_error(header+" Target '"+name+"' already exist!")
            self.mustdie(-1)
            return False

        if target.has_key("toolchain")==False:
            self.print_error(header+" Target: '"+name+"' has not set a toolchain")
            self.mustdie(-1)
            return False

        # check if toolchain HOST argument host will be replaced
        if target["toolchain"].find("host-") == False:
            if os.uname()[0].lower().find("linux") == 0:
                target["toolchain"] = target["toolchain"].replace("host", "linux")
            elif os.uname()[0].lower().find("cygwin") == 0:
                target["toolchain"] = target["toolchain"].replace("host", "cygwin")
            elif os.uname()[0].lower().find("mingw") == 0:
                target["toolchain"] = target["toolchain"].replace("host", "mingw")
            elif os.uname()[0].lower().find("darwin") == 0:
                target["toolchain"] = target["toolchain"].replace("host", "darwin")

        if target.has_key("cflags")==False:
            target["cflags"]=[]
        if target.has_key("asflags")==False:
            target["asflags"]=[]
        if target.has_key("cppflags")==False:
            target["cppflags"]=[]
        if target.has_key("ldflags")==False:
            target["ldflags"]=[]
        if target.has_key("libs")==False:
            target["libs"]=[]
        if target.has_key("defines")==False:
            target["defines"]=[]
        if target.has_key("undefines")==False:
            target["undefines"]=[]

        #lookup toolchain.name from toolchains table
        if self.toolchains.has_key(target["toolchain"])==False:
            self.print_error(header+" Target: '"+name+"' has non toolchain named: "+target["toolchain"])
            self.mustdie(-1)
            return False

        return True

    ## this method MUST NOT be called with create_targets==True
    # BEFORE toolchains/target has been read
    # from JSON file or commandline    because
    # only set create_targets to True if its the LAST call
    # means before makefile shall be generated
    def validate_and_create_project(self, create_all):
        if self.project.has_key("name")==False:
            self.project["name"]=os.path.basename(self.proj_dir)
        if self.project.has_key("build_dir")==False:
            self.project["build_dir"]="build"
        elif self.project.has_key("build_dir")==True:
            #we dont allow subdirs for now... we just ignore and replace
            if self.project["build_dir"].find("/")!=-1:
                self.print_warning("Subdirs are not allowed in build_dir I will replace it with '_'")
                self.project["build_dir"]=self.project["build_dir"].replace(os.sep, "_")
        if len(self.toolchains)==0 and create_all==True:
            self.add_toolchain({}, "native", False, header="Initialize failed")

        if len(self.targets)==0 and create_all==True:
            self.add_target({"toolchain" : "native"}, "default", False, header="Initialize failed")


        return True


    # FIXME: autogenerate toolchain depending on specifyied Target on runtime
    # when switch --set-target it should ask the system where toolchain exists
    # Developers shall never ever need to configure this
    # instead genmake shall:
    # 1. check what HOST OS genmake is runing on
    # 2. check for a already existing toolchain template (in /etc/genmake and ~/.genmake for HOSTOS
    # 3. copy contents from template to *_genmake.json
    # template shall be named toolchain_$(HOSTOS)_$(CPU)_$(TARGET_OS)_genmake.json and stored in ~/genmake/templates/(locally custom version) or /etc/genmake/templates/
    # where CPU is:
    # one of ["native","arm","avr8","avr32","68k"] and so on
    # supported Target OS:es are: "linux"(same as ""), "none-eabi"(also when FreeRTOS), "linux-gnueabi", or "windows"
    # and HOSTOS is one of: ["linux","macos","windows"]
    def validate_toolchain(self, toolchain, name, header, overwrite):
        name=name.lower()
        if name=="":
            self.print_error(header+" Toolchain must have a name, I Will abort")
            self.mustdie(-1)
            return False

        if self.toolchains.has_key(name)==True and overwrite==False:
            self.print_error(header+" Toolchain already exist '"+name+"', I Will abort")
            self.mustdie(-1)
            return False

        # FIXME: move this crap to a "template_default_toolchain"
        # this is platform dependent code especially paths
        prefix=""
        if toolchain.has_key("prefix")==True:
            prefix=toolchain["prefix"]
        if toolchain.has_key("bin")==False:
            if prefix=="":
                if os.name=="posix":
                    toolchain["bin"]="/usr/bin/"
                else:
                    toolchain["bin"]="C:\\Program Files"
            else:
                if os.name=="posix":
                    toolchain["bin"]="/toolchains/"+prefix[0:-1]+"/bin/"
                else:
                    toolchain["bin"]="C:\\Program Files"

            self.print_warning("Toolchain didn't specify a path to toolchain my guess: '"+toolchain["bin"]+"'!? change if needed")
        if toolchain.has_key("sys_includes")==False:
            # fiexme check if target is host and add /usr/include if thats the case
            toolchain["sys_includes"]=[]
        if toolchain.has_key("cc")==False:
            self.print_verbose("Toolchain didn't specify a C compiler'"+prefix+"gcc' will be added as default")
            toolchain["cc"]=toolchain["bin"]+prefix+"gcc"
            toolchain["cpp"]=toolchain["bin"]+prefix+"g++"
        if toolchain.has_key("as")==False:
            toolchain["as"]=toolchain["bin"]+prefix+"as"
            self.print_verbose("Toolchain didn't specify a assembler '"+toolchain["as"]+"' will be added as default")
        if toolchain.has_key("size")==False:
            toolchain["size"]=toolchain["bin"]+prefix+"size"

        #both ld and linker are allowed ld has will overwrite linker
        if toolchain.has_key("ld")==True:
            toolchain["linker"]=toolchain["ld"]
            del toolchain["ld"]
        if toolchain.has_key("linker")==False:
            toolchain["linker"]=toolchain["bin"]+prefix+"gcc"
            self.print_verbose("Toolchain didn't specify a linker '"+toolchain["linker"]+"' will be added as default")
        if toolchain.has_key("platform")==False:
            toolchain["platform"]=os.name
            self.print_verbose("Toolchain didn't specify a platform defaults to native in this case: '"+toolchain["platform"]+"'")
        if toolchain.has_key("objcopy")==False:
            toolchain["objcopy"]=toolchain["bin"]+prefix+"objcopy"
            self.print_verbose("Toolchain didn't specify a objcopy defaults to: '"+toolchain["objcopy"]+"'")
        if toolchain.has_key("objdump")==False:
            toolchain["objdump"]=toolchain["bin"]+prefix+"objdump"
            self.print_verbose("Toolchain didn't specify a objdump defaults to: '"+toolchain["objdump"]+"'")
        if toolchain.has_key("texcompile")==False:
            toolchain["texcompile"]="latex"
            self.print_verbose("Toolchain didn't specify a tex/latex compiler defaults to: '"+toolchain["texcompile"]+"'")
        if toolchain.has_key("dvipdf")==False:
            toolchain["dvipdf"]="dvipdf"
            self.print_verbose("Toolchain didn't specify a dvipdf defaults to: '"+toolchain["dvipdf"]+"'")
        if toolchain.has_key("pkgconfig")==False:
            # pkgconfig shall use toolchain dirs config
            toolchain["pkgconfig"]=toolchain["bin"]+"pkg-config"
            self.print_verbose("Toolchain didn't specify a pkgconfig defaults to: '"+toolchain["pkgconfig"]+"'")
        if toolchain.has_key("dvipdf")==False:
            toolchain["dvipdf"]="dvipdf"
            self.print_verbose("Toolchain didn't specify a dvipdf defaults to: '"+toolchain["dvipdf"]+"'")
            # this is what we pass to objcopy
        if toolchain.has_key("git")==False:
            toolchain["git"]="git"
            self.print_verbose("Didn't specify a git, defaults to: '"+toolchain["git"]+"'")
        if toolchain.has_key("astyle")==False:
            if os.path.isfile("/usr/bin/astyle"):
                toolchain["astyle"]="astyle"
            else:
                toolchan["astyle"] = "echo"

            self.print_verbose("Didn't specify a astyle, defaults to: '"+toolchain["astyle"]+"'")

        self.print_verbose ("toolchain '"+name+"' is valid")
        return True


    def jsonfile_is_valid(self, indata):
        if indata.has_key("project")==False or indata.has_key("targets")==False or indata.has_key("toolchains")==False:
            self.print_error(fname+" must include project, targets and toolchains objects")
            self.mustdie(-9)
            return False

        if type(indata["project"])!=dict or type(indata["targets"])!=dict or type(indata["toolchains"])!=dict:
            self.print_error("Error '"+fname+"' project, targets and toolchains must of type(Object) means: project: {....} NOT project : [...] and NOT project: \"...\"")
            self.mustdie(-10)
            return False

        return True

    ## read_json
    # !!!do not call this before read commandline args!!!
    def read_json(self, fname):
        f=open(fname, "r")
        data=f.read()
        f.close()
        indata=json.JSONDecoder().raw_decode(data)[0]

        if self.jsonfile_is_valid(indata)==False:
            return self.die

        self.project=indata["project"]
        if self.arg_current_target!="":
            self.project["current_target"]=self.arg_current_target
            self.save = True

        #call validate so to check to se that it hass needed field and add them if
        #not in place but create_all=False because we dont want to create
        #tools/targets yet. We push them in below
        self.validate_and_create_project(create_all=False)

        #this looks ugly but we have to lookup key AND values who actually is a Hash to
        #key also be the name in the added toolchain
        for name, toolchain in indata["toolchains"].items():
            self.add_toolchain(toolchain, name, overwrite=False, header="file: '"+fname+"'" )

        for name, target in indata["targets"].items():
            self.add_target(target, name, overwrite=False, header="file: '"+fname+"'" )

        #now we can create targets/toolchains if not already created
        self.validate_and_create_project(create_all=True)
        self.print_debug(self.dump())
        self.pkgconfigs=[]
        return True

    def dump(self):
        #FIXME
        #possible can be done better but the python JSON API is not well documented
        #or maybe I am just blind
        gen={}
        gen["project"]=self.project
        gen["toolchains"]=self.toolchains
        gen["targets"]=self.targets
        return json.dumps(gen, indent=2)

    def pkgconfig_add_dependies(self, argv):
        for arg in argv:
            if arg not in self.pkgconfigs:
                self.print_debug("pkgconf "+arg)
                self.pkgconfigs.append(arg)

    def search_for_json(self):
        # dont iterate if we want to create
#        if os.path.isfile(self.json_file) == False and self.create_json==True:
#            return False

        fname=self.json_file
        #are we in root? means "/blabla_genmake.json"
        while fname.count(os.path.sep) > 1:
            if os.path.isfile(fname) == True:
                if self.create_json==False and fname!=self.json_file:
                    self.print_warning("Didn't find file: '"+self.json_file+"' However I found one att parent location I will use: '"+fname+"'.\n")
                    self.print_verbose("(If you want to create a genmake in: '"+self.json_file+"' use commandline switch --create.)")
                elif self.create_json==True and fname!=self.json_file:
                    self.print_warning("Notice! subprojects may lead to bugs if you don't configure parent gemmake correctly!\n(You should add \"exclude_path\" : \""+os.path.dirname(self.json_file)+"\" to your: '"+fname+"'")
                    return False

                # parent or file found json_File now set to parent if we dont want
                # creste as subproject
                if self.create_json==False:
                    self.json_file=fname

                return True

            # strip basename and make new path+basename
            basename=os.path.basename(fname)
            path=os.path.dirname(fname)
            fname=os.path.abspath(path+os.path.sep+"..")
            fname=fname+"genmake.json"
            if os.path.dirname(fname)==os.path.sep:
                break

        return False


    ## check existense of basename_genmake.json
    # create it if --create has been specified or ask if --interactive
    # @return True on success or @error on error and sets self.die to errorcode
    # 2.1 if ERROR then read from default template and save
    # 2.2 if OK merge commandline version
    def check_if_jsonfile_exists_and_allow_create(self):
        if self.search_for_json()==False:
            # TODO: cd .. to check if genmake exists in parent
            yes=None
            if self.interactive == True:
                self.print_error ("File: '"+self.json_file+"' Don't exist")
                print("shall I create it for you (y/n)?"),
                yes=raw_input()
            elif self.create_json == True:
                self.print_verbose ("File: '"+self.json_file+"' Don't exist but I will create it")
                yes="y"
            else:
                self.print_error ("File: '"+self.json_file+"' Don't exist. use --create if you want me to create a default one for you")
                self.mustdie(-3)
                return False

            if yes==None or yes.lower().find("y")!=0:
                self.mustdie(-4)
                return False
            else:
                # if we come here whe can be sure there is no file
                # so we can call method with create_all=True
                self.validate_and_create_project(create_all=True)
                try:
                    f=open(self.json_file, "w+")
                    f.write(self.dump())
                    f.close()
                except:
                    self.print_error("Couldn't create: '"+self.json+"'")
                    return False
        else: # file exists read it
            #read file
            try:
                self.read_json(self.json_file)
            except:
                self.print_error("Failed to read/parse: '"+self.json_file+"'");
                if self.debug==True:
                    raise
                return self.mustdie(-1)

        # if we come here its good the file exists :)
        return True

    # 1 parse commandline args
    # 2 try read json "projectfile"
    # 3 create makefile
    # 4 save jsob file if changed
    def run(self):
        #parse args can override defaults no callback set because we are
        #inherited from Application
        self.parse_args(None)
        self.print_verbose("Curdir set to: "+self.proj_dir)
        self.json_file=self.proj_dir+os.sep+"genmake.json"
        if self.check_if_jsonfile_exists_and_allow_create()==False:
            return self.die

        # FIXME ugly
        target = self.targets[app.project["current_target"]]
        self.readdir.exclude_dirs=target["exclude_dirs"]
        self.readdir.ignores.append(self.proj_dir+app.project["build_dir"]+"/"+app.project["current_target"])
        if self.readdir.run(self.proj_dir)==False:
            return self.mustdie(-1)

        if target.has_key("extra_sources") == True:
            for item in target["extra_sources"]:
                self.readdir.run(item)

        if self.save == True:
            f=open(self.json_file, "w+")
            f.write(self.dump())
            f.close()

        return self.generate_makefile()

    def generate_makefile(self):
        #setup current toolchain and target
        target=self.targets[self.project["current_target"]]
        if target.has_key("parent") == True:
            parent = self.targets[target["parent"]]
            self.print_verbose("has parent target I will copy settings")
            for item in parent["include_dirs"]:
                if item not in target["include_dirs"]:
                    target["include_dirs"].append(item)
            for item in parent["target_pre_rule"]:
                if item not in target["target_pre_rule"]:
                    target["target_pre_rule"].append(item)
            for item in parent["target_post_rule"]:
                if item not in target["target_post_rule"]:
                    target["target_post_rule"].append(item)
	    for item in parent["libs"]:
                if item not in target["libs"]:
                    target["libs"].append(item)
 	    for item in parent["ldflags"]:
                if item not in target["ldflags"]:
                    target["ldflags"].append(item)
            for item in parent["asflags"]:
                if item not in target["asflags"]:
                    target["asflags"].append(item)
            for item in parent["cflags"]:
                if item not in target["cflags"]:
                    target["cflags"].append(item)
            for item in parent["defines"]:
                if item not in target["defines"]:
                    target["defines"].append(item)
            for item in parent["undefines"]:
                if item not in target["undefines"]:
                    target["undefines"].append(item)
            for item in parent["exclude_dirs"]:
                if item not in target["exclude_dirs"]:
                    target["exclude_dirs"].append(item)


	print(target)
        for item in self.readdir.include_dirs:
            print(":"+item)
            if item not in target["include_dirs"] and self.is_exclude_dir(target["exclude_dirs"], item) == False:
                target["include_dirs"].append(item)

        toolchain=self.toolchains[target["toolchain"]]
        if self.makefile.run(self.project, self.proj_dir, target, toolchain)==0:
            if os.path.isfile(self.json_file) == False:
                f=open(self.json_file, "w+")
                f.write(self.dump())
                f.close()
            else:
                f=open(self.json_file.replace("genmake.json", "local_genmake.json"), "w+")
                f.write(self.dump())
                f.close()

    def is_exclude_dir(self, excludes, path):
	return False
        for exc in excludes:
            print(exc+" "+str(path.find(exc)))
            if path.find(exc) == 0:
                print("excluded: "+path)
                return True

        return False




app=Manager(VERSION)
res=app.run()
if app.die!=0:
    app.print_error("Aborted: "+str(res))
else:
    app.print_ok("Exited succefully")

sys.exit(res)
