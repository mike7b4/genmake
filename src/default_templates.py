template_rule_latex_commands={ "generate_file" : ".pdf",
                                "commands" : [
                                ["$(LATEX)","$(FILE)"],
                                ["$(PDFLATEX)", "$(FILE_NO_PREFIX).dvi"]
                                ]
                              }


template_rule_genmake_commands={ "generate_file" : "Makefile",
                                "commands" : [
				    ["cd $(PROJ_DIR) ; $(GENMAKE_EXE)","$(PROJ_DIR)"],
				    ["@echo \"=== Genmake regenerated Makefile ===\""],
                    ["@echo \"=== because of that i do make clean ===\""],
                    ["@make clean"],
				    ["@exit"]
                                ]
                              }


mdeps="""

# Automatically generate C source code dependencies.
# (Code originally taken from the GNU make user manual and modified
# (See README.txt Credits).)
#
# Note that this will work with sh (bash) and sed that is shipped with WinAVR
# (see the SHELL variable defined above).
# This may not work with other shells or other seds.
#
%.d: %.c
\tset -e; $(CC) -MM $(ALL_CFLAGS) $< \\
\t| sed 's,\(.*\)\.o[ :]*,\\1.o \\1.d : ,g' > $@; \\
\t[ -s $@ ] || rm -f $@


# Remove the '-' if you want to see the dependency files generated.
-include $(SRC:.c=.d)"""
