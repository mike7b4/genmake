#!/usr/bin/python
# kate: space-indent on; indent-width 4; replace-tabs on
import sys
import os
import fnmatch
import json
from application import *
from parsefile import *
class ReadDir:
    #dont read QT moc_*/qrc_*.cpp files here they will be added later if a H file
    #includes "Q_OBJECT"
    def __init__(self, app, exclude_dirs=[], ignores=[".git", ".svn","moc_*.cpp", "qrc_*.cpp"], allowed=["*.c", "*.cpp", "*.h", "*.s","*.tex", "*.asm","*.qrc"]):
        self.app=app
        self.ignores = ignores
        #kinda dirty doing this way
        self.allowed_source_files=allowed
        self.c_sources=[]
        self.cpp_sources=[]
        self.tex_sources=[]
        self.assembler_sources=[]
        self.headers=[]
        self.include_dirs=[]
        self.exclude_dirs=exclude_dirs
        self.objects=[]

    def dump(self):
        #FIXME
        #possible can be done better but the python JSON API is not well documented
        #or maybe I am just blind
        gen={}
        gen["objects"]=self.objects
        gen["c_sources"]=self.c_sources
        gen["cpp_sources"]=self.cpp_sources
        gen["assembler_sources"]=self.assembler_sources
        gen["headers"]=self.headers
        gen["include_dirs"]=self.include_dirs
        return json.dumps(gen, indent=2)

    def is_ignored_file(self, fil):
        for match in self.ignores:
            if fnmatch.fnmatch(fil, match):
                self.app.print_verbose("ignored:"+fil)
                return True

        return False

    # check if allowed
    def is_denied_dir(self, fil):
        for relative in self.exclude_dirs:
            match = os.path.abspath(relative)
            a = fil.split(match)
            print("split:"+str(a)+"=="+str(match))
            if len(a) > 1:
                print("Excluded dir: '"+fil)
                return True

        return False

   # check if allowed
    def is_allowed_file(self, fil):
        for match in self.allowed_source_files:
            if fnmatch.fnmatch(fil, match):
                 return True

        return False

    def check_file(self, fil, rpath, absfile, beenhere):
        if fnmatch.fnmatch(fil, "*.tex"):
            self.tex_sources.append(absfile)
        elif fnmatch.fnmatch(fil, "*.h") or fnmatch.fnmatch(fil, "*.qrc"):
            self.headers.append(absfile)
            if beenhere == False:
                beenhere = True
                self.include_dirs.append(rpath)
        elif fnmatch.fnmatch(fil, "*.s") or fnmatch.fnmatch(fil, "*.S") or fnmatch.fnmatch(fil, "*.asm"):
            self.assembler_sources.append(absfile)
        elif fnmatch.fnmatch(fil, "*.cc") or fnmatch.fnmatch(fil, "*.cpp"):
            self.cpp_sources.append(absfile)
        else:
            self.app.print_verbose("add: "+absfile)
            self.c_sources.append(absfile)

        return beenhere

    def run(self, rpath, firstround=True):
        apath=os.path.abspath(rpath)
        try:
            rdir=os.listdir(rpath)
        except:
            if firstround==True:
                self.app.print_error("Directory '"+apath+"' does not exist/is not a dir or permission denied")
                return False
            else:
                self.app.print_warning("Directory '"+apath+"' does not exist/is not a dir or permission denied")
                return True

        beenhere=False
        for fil in rdir:
            absfile=apath+os.sep+fil
            # ignore file or dir if ignore list says so
            denied = self.is_ignored_file(fil)
            isdir = os.path.isdir(absfile)
            allowed = True
            if isdir:
                denied = self.is_denied_dir(absfile)
                allowed = not denied
            else:
                allowed  = self.is_allowed_file(fil)

            if (isdir == True and denied == False) or allowed == True and denied==False:
                if isdir:
                    self.run(rpath+os.sep+fil, False)
                else:
                    beenhere = self.check_file(fil, rpath, absfile, beenhere)
            else:
                self.app.print_verbose("file/dir '%s' was filtered allowed == %s, denied == %s" % (absfile, allowed, denied))

        return True

def cb_next_arg(app, arg):
    if os.path.isdir(os.path.abspath(arg))==True:
        app.filename=os.path.abspath(arg)

if __name__=="__main__":
    app=Application()
    app.filename=None
    app.parse_args(cb_next_arg)
    r=ReadDir(app)
    if app.filename==None:
        app.filename=os.path.abspath(os.curdir)

    r.run(app.filename)
    js={}
    js["ass_sources"]=r.assembler_sources
    js["c_sources"]=r.c_sources
    js["cpp_sources"]=r.cpp_sources
    js["headers"]=r.headers
    js["include_dirs"]=r.include_dirs
    for fil in r.sources:
        parse=ParseFile(app)
        parse.run(fil)
        app.print_verbose (parse.dump())

    app.print_verbose (json.dumps(js, indent=2))

