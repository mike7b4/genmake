#!/usr/bin/python
# kate: space-indent on; indent-width 4; replace-tabs on

import sys
import os
import fnmatch
import subprocess
import datetime
import shutil
from hashlib import md5
from parsefile import *
from default_templates import *
class Makefile():
    def __init__(self, app):
        self.app = app
        self.build_dir=""
        self.readdir=self.app.readdir
        self.extra_rules=[]
        self.rule_all=""
        self.target=None
        self.cygwin = False

    # wrapper to make it work with cygwin
    def proj_replace(self, obj, replace_me):
        if self.cygwin == True:
            return obj.replace(replace_me, "$(PROJ_DIR_CYGWIN)"+os.path.sep)
        else:
            return obj.replace(replace_me, "$(PROJ_DIR)"+os.path.sep)

    def rule_translate_commands(self, rule, fname):
        if rule.has_key("commands")==False or rule.has_key("generate_file")==False:
            self.app.print_warning("rule: "+rule, "Is missing key argument(s) 'commands' and 'generate_file' must exist")
            return False

        out=""
        gfile=rule["generate_file"]
        if gfile[0]==".":
            # user meant this:
            gfile="$(FILE_NO_PREFIX)"+gfile

        fname,ext=os.path.splitext(fname)
        #shortcut the projdir
        infile=fname.replace(self.proj_dir,"$(PROJ_DIR)")+ext
        # always build target in builddir
        gfile=gfile.replace("$(FILE_NO_PREFIX)", fname).replace("$(PROJ_DIR)","")
        gfile=os.path.basename(gfile)
        if gfile.find("$(FILE)")!=-1:
            self.app.print_warning("$(FILE) in KEY: generate_file is WRONG! Ignored... will be passed raw to Makefile")
            self.app.print_debug(rule)

        out=gfile+": "+infile
        for command in rule["commands"]:
            out=out+"\n\t"
            out=out+(" ".join(map(str, command)))
            out=out.replace("$(GENERATE_FILE)", gfile)
            out=out.replace("$(FILE)", infile)
            out=out.replace("$(FILE_NO_PREFIX)", os.path.basename(fname))

        return (gfile, out)

    ## generate_makefile
    #generates makefile for current target
    #the JSON file must already been readed before call this method
    #return 0 on succes or negative on error
    def run(self, project, proj_dir, target, toolchain):
        self.target = target
        self.app.print_verbose("makefile for "+str(target))
        self.proj_dir = proj_dir
        self.project = project
        platform = toolchain["platform"]
        builddir = self.app.proj_dir+os.sep+project["build_dir"]+os.sep+project["current_target"]+os.sep
        rbuilddir = self.app.project["build_dir"]+os.path.sep+project["current_target"]+os.path.sep
        mfile = builddir+"Makefile"
        workdir = self.app.proj_dir
        try:
            os.makedirs(builddir)
        except:
            self.app.print_verbose("Could not create "+builddir+" check if already exists...")
            if os.path.isdir(builddir)==False:
                self.app.print_error("Could not create directory: '"+builddir+"'")
                return self.app.mustdie(-4)

        try:
            f=open(mfile, "w+")
        except:
            self.app.print_error("What!? Could not create Makefile :O")
            return self.app.mustdie(-5)

        self.add_file_header(f, target)

        f.write("# Target\n\n")
        f.write("GENMAKEFILE="+self.app.json_file+"\n")
        f.write("TARGET="+self.project["current_target"]+"\n")
        f.write("TARGET_NO_PREFIX="+self.project["current_target"].split(".")[0]+"\n")
        if os.uname()[0].lower().find("cygwin") != -1:
            self.cygwin = True
            f.write("PROJ_DIR ="+self.proj_dir+os.path.sep+"\n")
            f.write("PROJ_DIR_CYGWIN = $(shell cygpath -m $(PROJ_DIR))\n")
        elif os.uname()[0].lower().find("mingw") != -1:
            self.print_error("MINGW: FIXME do we need to tweak the path in MINGW like CYGWIN? Please check makefile.py and remove this line if it works!")
            f.write("PROJ_DIR="+self.proj_dir+os.path.sep+"\n")
            if self.app.debug == True:
                sys.exit(-1)
        else:
            f.write("PROJ_DIR="+self.proj_dir+os.path.sep+"\n")

        f.write("BUILD_DIR="+rbuilddir+"\n")

        f.write("# Toolchain stuff\n\n")
        f.write("GENMAKE_EXE="+os.path.abspath(sys.argv[0])+"\n")
        f.write("ASTYLE="+toolchain["astyle"]+"\n")
        f.write("CC="+toolchain["cc"]+"\n")
        f.write("CPP="+toolchain["cc"]+"\n")
        f.write("AS="+toolchain["as"]+"\n")
        f.write("LD="+toolchain["linker"]+"\n")
        f.write("OBJCOPY="+toolchain["objcopy"]+"\n")
        f.write("OBJDUMP="+toolchain["objdump"]+"\n")
        f.write("SIZE="+toolchain["size"]+"\n")
        if len(self.readdir.tex_sources)!=0:
            f.write("LATEX="+toolchain["texcompile"]+"\n")
            f.write("PDFLATEX="+toolchain["dvipdf"]+"\n")

        f.write("PKGCONFIG="+toolchain["pkgconfig"]+"\n")

        f.write("ASTYLE_FLAGS="+(" ").join(toolchain["astyle_flags"])+"\n")
        f.write("\n\n# defines\n\n")
        f.write("DEFINES=")
        for define in target["defines"]:
            f.write("\t\t\\\n\t-D"+define)

        f.write("\n\n# undefined\n\n")
        f.write("UNDEFINES=")
        for undef in target["undefines"]:
            f.write("\t\t\\\n\t-U"+undef)

        f.write("\n\n# sysincludes directives\n\nSYS_INCLUDE_DIRS=")
        for fil in toolchain["sys_includes"]:
            #make i relative
            f.write("\t\t\\\n\t-I"+fil)


        #TODO: make a lookuptable for this mess!!!!
        if platform=="windows":
            suffix=".exe"
        elif platform=="coldfire" or platform=="arm" or platform=="avr8" or platform=="avr32":
            suffix=".elf"
        else:
            #unix and others
            suffix=""

        targetname,rule=self.rule_translate_commands(template_rule_genmake_commands, "$(GENMAKEFILE)")
        self.rule_all=" Makefile prepare "+self.rule_all
        self.extra_rules.append(rule)

        #must add headers before any of below because
        #make headers may add more cpp files if QT
        self.add_headers(f, target, toolchain)
        self.add_flags(f, target)
        self.add_c_sources(f)
        self.add_cpp_sources(f)
        self.add_assembler_sources(f)
        self.add_tex_sources(f)
        self.add_objects(f, "$(TARGET)"+suffix)
        #build all
        f.write("\n\nall:"+self.rule_all+"\n")
        f.write("\t@echo \"All done\"\n")

        f.write("\n\nprepare:\n")
        if target.has_key("target_pre_rule") and len(target["target_pre_rule"])!=0:
            for item in target["target_pre_rule"]:
                f.write("\t"+(" ").join(item)+"\n")
        else:
            f.write('\t@echo ""\n')

        if len(self.readdir.objects) > 0:
            f.write("\n\n$(TARGET)"+suffix+": $(OBJECTS)\n")
            f.write("\t$(LD) $(LDFLAGS) -o $(TARGET)"+suffix+" $(OBJECTS) $(LIBS)\n")
            for item in target["target_post_rule"]:
		print(item)
                f.write("\t"+item+"\n")
            f.write("\t$(SIZE) $(TARGET)"+suffix+"\n")
            f.write("\tcp $(TARGET)"+suffix+" ../..")
        #makefile
        f.write("\n\nclean:\n")
        f.write("\trm -f *.o *.elf *.map *.hex *.s19 *.lst *.d moc_*.cpp qrc_*.cpp $(TARGET)")

        self.add_rules(f)
        self.add_extra_rules(f)

        f.write("\n\n.PHONY: all clean\n")

        f.write("\n\n\n# End of Makefile\n")

        f.close()
        f=open("current_target_debug.json", "w+")
        f.write(self.readdir.dump())
        f.close()

        #end of run()
        return 0

    ## make file header with some Basic information
    def add_file_header(self, f, target):
        f.write("#############################################################\n")
        f.write(("# This is an Autogenerated GNU Makefile").ljust(60)+"#\n")
        f.write(("# Using GenMakeFile, Version "+self.app.version).ljust(60)+"#\n")
        f.write(("# Copyright 2012, 2013 - Mikael Hermansson").ljust(60)+"#\n")
        f.write(("# genmake is GPLv2").ljust(60)+"#\n")
        f.write(("# "+datetime.datetime.now().isoformat()).ljust(60)+"#\n")
        f.write(("#").ljust(60)+"#\n");
        f.write(("# Generated for target: '"+self.project["current_target"]+"'").ljust(60)+"#\n")
        f.write(("# Using toolchain: '"+target["toolchain"]+"'").ljust(60)+"#\n")
        f.write(("#").ljust(60)+"#\n");
        f.write(("# Here we go :)").ljust(60)+"#\n")
        f.write("#############################################################\n\n")

    def is_exclude_dir(self, fil):
        for f in self.target["exclude_dirs"]:
            st=fil[0:len(f)]
            self.app.print_verbose("exclude?="+st)
            self.app.print_verbose(f)
            if st == f:
                self.app.print_verbose("exclude"+fil)
                return True

        return False

    ## Add headers
    # @param f filepointer
    # @param target current target pointer
    # @param toolchain current
    def add_headers(self, f, target, toolchain):
        if len(self.readdir.headers)==0:
            return False

        f.write("\n\n\n# Header files in project\n\n")
        f.write("HEADERS=")
        index=-1
        for fil in self.readdir.headers:
            index+=1
            if self.is_exclude_dir(fil):
                continue

            fp=ParseFile(self.app)
            err=fp.run(fil)
            if err!=0:
                return err

            if not self.project["current_target"] in fp.exclude_targets:
                f.write("\t\t\\\n\t"+self.proj_replace(fil, self.proj_dir+os.sep))
                if fp.fileinfo["call_qtmoc"]==True:
                    #make a rule for MOC compiler
                    moccpp="moc_"+fil.replace(self.proj_dir+os.sep,"").replace(".h",".cpp")
                    header=self.proj_replace(fil, self.proj_dir+os.sep)
                    self.readdir.cpp_sources.append(moccpp)
                    rule=moccpp+": "+header+"\n"
                    #FIXME hardcoded moc
                    rule+="\tmoc-qt4 "+header+" -o "+moccpp+"\n"
                    self.extra_rules.append(rule)
                elif fp.fileinfo["call_rcc"]==True:
                    #make a rule for MOC compiler
                    qrccpp="qrc_"+fil.replace(self.proj_dir+os.sep,"").replace(".qrc",".cpp")
                    header=self.proj_replace(fil, self.proj_dir+os.sep)
                    self.readdir.cpp_sources.append(qrccpp)
                    rule=qrccpp+": "+header+"\n"
                    #FIXME hardcoded rcc
                    rule+="\trcc "+header+" -o "+qrccpp+"\n"
                    self.extra_rules.append(rule)
            else:
                # it was excluded remove from json
                self.app.print_verbose("Excluded: "+fil)
                del self.readdir.headers[index]

        f.write("\n")
        # OMG! this is a mess
        # check if we have c or cpp sources in project
        if len(self.readdir.c_sources)>0 or len(self.readdir.cpp_sources)>0:
            # check if we got any pkg config when parsed header files
            if len(self.app.pkgconfigs)>0:
                self.app.print_verbose("we got some pkgconfigs")
                deps=" ".join(map(str, self.app.pkgconfigs))
                try:
                    cmd=[toolchain["pkgconfig"], "--cflags"]+self.app.pkgconfigs
                    # pkg-config adds two whitespaces at end of first line
                    cflags=subprocess.check_output(cmd).split("    \n")[0]
                    cmd=[toolchain["pkgconfig"], "--libs"]+self.app.pkgconfigs
                    libs=subprocess.check_output(cmd).split("    \n")[0]
                    self.app.print_verbose("pkg-config added: "+libs)
                except:
                    if self.app.debug==True:
                        raise
                    self.print_error("pkgconfig could not add flags :(")
                    cflags=""
                    libs=""

                # check no dups
                #FIXME: pkgconfig --cflags adds includedirs this should be passed
                #directly to $INCLUDE_DIRS instead to make it look cleaner in Makefile
                for cflag in cflags.split(" "):
                    if cflag.find("-I")==0:
                        cflag = cflag.split("-I")[1]
                        if not cflag in self.readdir.include_dirs:
                            self.readdir.include_dirs+=[cflag]
                    elif not cflag in target["cflags"]:
                        target["cflags"]+=[cflag]
                # check no dups
                for lib in libs.split(" "):
                    if not lib in target["libs"]:
                        target["libs"]+=[lib]

        f.write("\n\n# include directives\n\nINCLUDE_DIRS=")
        for fil in target["include_dirs"]:
            #make i relative
            rel=self.proj_replace(fil, self.proj_dir+os.sep)
            if rel=="":
                f.write("\t\t\\\n\t-I.")
            else:
                f.write("\t\t\\\n\t-I"+rel)

        f.write("\n\n")

    def add_flags(self, f, target):
        if len(self.readdir.cpp_sources)>0 or len(self.readdir.c_sources)>0:
#             f.write("\n\n# Compiler and linker flags\n\n")
            f.write("LDFLAGS="+" ".join(map(str,target["ldflags"]))+"\n")
            f.write("LIBS="+" ".join(map(str,target["libs"]))+"\n")
            f.write("ASFLAGS="+" ".join(map(str,target["asflags"]))+"\n")
            f.write("CFLAGS="+" ".join(map(str,target["cflags"]))+"\n")
            f.write("ALL_CFLAGS=$(UNDEFINES) $(DEFINES) $(CFLAGS) $(SYS_INCLUDE_DIRS) $(INCLUDE_DIRS)\n\n")

        if len(self.readdir.cpp_sources)>0:
            f.write("CXXFLAGS="+" ".join(map(str,target["cppflags"]))+"\n")
            f.write("ALL_CXXFLAGS=$(UNDEFINES) $(DEFINES) $(CXXFLAGS) $(SYS_INCLUDE_DIRS) $(INCLUDE_DIRS)\n\n")

        f.write("\n\n")

        return True


    # check target filter check every file if it has been excluded/or included from currentrunning target
    def check_target_filters(self, fp):
        if self.app.lookup(fp.include_targets, self.project["current_target"]) == True:
            return True
        elif self.app.lookup(fp.exclude_targets, self.project["current_target"]) == True:
            return False

        # fallback if there is no include targets specified in json and not excluded we take it as file shall be compiled
        elif len(fp.include_targets) == 0:
            return True

        return False

    ### TODO merge *_sources together to one method
    # almost all is doing the same stuff better check
    #    fileinfo["..."] what filetype etc...
    #    then we can add special buildrules direcly in the files to :)
    #    JSON: genmake    {
    #                                     "file_target" : ".pdf",
    #                                     "commands" :
    #                                         [
    #                                            ["latex","$(FILENAME)"],
    #                                            ["dvipdf"],["$(FILENAME_NO_PREFIX).dvi"]
    #                                        ]
    #                                    }
    # the created rule:
    # where $FILENAME is auto replaced with current filename
    # example if filename is: "foo.tex"
    # the generated target will be like:
    # foo.pdf: foo.tex
    #         latex foo.tex
    #         dvipdf foo.dvi
    def add_tex_sources(self, f):
        if len(self.readdir.tex_sources)==0:
            return False

#        f.write("\n\n\n# Latex Sources (only for debug purpose)\n")
#        f.write("LATEX_SRC=")
        index=-1
        for fil in self.readdir.tex_sources:
            index+=1
            if self.is_exclude_dir(fil):
                self.app.print_verbose("exdirectory: "+fil)
                continue

            fp=ParseFile(self.app)
            err=fp.run(fil)
            if err!=0:
                return err

            if self.check_target_filters(fp) == True:
                targetname,rule=self.rule_translate_commands(template_rule_latex_commands, fil)
                self.extra_rules.append(rule)
                self.rule_all+=" "+targetname
            else:
                # it was excluded remove from json
                self.app.print_verbose("Excluded: "+fil)
                del self.readdir.cpp_sources[index]


    ## Add Assembler Sourcefiles
    # @param f filename
    def add_assembler_sources(self, f):
        if len(self.readdir.headers)==0:
            return False

        f.write("\n\n\n# Assembler Sources\n")
        f.write("AS_SRC=")
        index=-1
        for fil in self.readdir.assembler_sources:
            index+=1

            fp=ParseFile(self.app)
            err=fp.run(fil)
            if err!=0:
                return err

            if self.is_exclude_dir(fil):
                pass
            elif self.app.lookup(fp.exclude_targets, self.project["current_target"])==False:
                src=self.proj_replace(fil, self.proj_dir+os.sep)
                f.write("\t\t\\\n\t"+self.proj_replace(fil, self.proj_dir+os.sep))
                self.readdir.objects.append(src.replace(".s","_s.o"))
            else:
                # it was excluded remove from json
                self.app.print_verbose("Excluded: "+fil)
                del self.readdir.assembler_sources[index]


    ## add C files (if there is)
    # @brief f fileobject
    def add_c_sources(self, f):
        if len(self.readdir.c_sources)==0:
            return False

        f.write("# The C files\n\n")
        f.write("C_SRC=")
        #FIXME: the and exlude should be done befor create Makefile?
        self.app.print_verbose("add_c_sources files in: "+("\n").join(self.readdir.c_sources)+" =======================")
        # make a copy of the list and iterate it remove from original excluded from original if needed
        sources = [ item for item in self.readdir.c_sources  ]
        for fil in sources:
            self.app.print_verbose("source: "+str(fil))
            fp=ParseFile(self.app)
            self.app.print_debug ("file '%s' include_targets %s" % (fil,str(fp.include_targets)))
            err=fp.run(fil)
            if err!=0:
                self.app.print_verbose("err "+fil)
                return err
            # not in exlude list?
            if self.is_exclude_dir(fil):
                self.app.print_verbose("Excluded: "+str(fil))
            elif self.check_target_filters(fp) == True:
                src=self.proj_replace(fil, self.proj_dir+os.sep)
                f.write("\t\\\n\t"+src)
                self.readdir.objects.append(src.replace(".c","_c.o"))
            else:
                #it was excluded remove from json
                self.app.print_verbose("Excluded: "+fil)
                self.readdir.c_sources.remove(fil)

        return True


    ## Add CPP files
    # @brief f fileobject
    def add_cpp_sources(self, f):
        if len(self.readdir.cpp_sources)==0:
            return False

        f.write("\n\nCPP_SRC=")
        #FIXME: the and exlude should be done befor create Makefile?
        index=-1
        for fil in self.readdir.cpp_sources:
            index+=1
            fp=ParseFile(self.app)
            err=fp.run(fil)
            if err!=0:
                return err
            # not in exlude list?
            if self.app.lookup(fp.exclude_targets, self.project["current_target"])==False:
                src=self.proj_replace(fil, self.proj_dir+os.sep)
                f.write("\t\\\n\t"+src)
                #FIXME wrong place
                self.readdir.objects.append(src.replace(".cpp","_cpp.o"))
            else:
                #it was excluded remove from json
                self.app.print_verbose("Excluded: "+self.readdir.cpp_sources[index])
                del self.readdir.sources[index]

    ## all the .o files
    def add_objects(self, f, rulename):
        if len(self.readdir.objects)==0:
            return False

        self.rule_all+=" "+rulename
        f.write("\n\nOBJECTS=")
        #FIXME: the and exlude should be done befor create Makefile?
        index=-1
        lastline=""
        for fil in self.readdir.objects:
            # not in exlude list?
            f.write("\t\\\n\t"+md5(fil.replace("$(PROJ_DIR)/", "")).hexdigest()+"_"+os.path.basename(fil).replace(" ", "_"))

    ## The standard rules probadly only works for GCC
    # TODO: add other compilers
    def add_rules(self, f):
        if len(self.readdir.objects)==0:
            return False

        f.write("\n\n# lets add some rules....\n\n")
        #FIXME: the and exlude should be done befor create Makefile?
        index=-1
        lastline=""
        for fil in self.readdir.objects:
            index+=1
            # not in exlude list?
            ofile=fil.replace("$(PROJ_DIR)/", "")
            ofile=md5(ofile).hexdigest()+"_"+os.path.basename(fil).replace(" ", "_")#+".o"
            f.write(ofile)
            f.write(": ")
            #Dirty little trick
            #this may harm your windows machine ;)
            infile=fil.replace("_cpp.o",".cpp").replace("_c.o",".c").replace("_asm.o",".asm").replace("_S.o",".S").replace(".cc.o",".cc").replace("_s.o", ".s")
            f.write(infile)
            f.write("\n")
            #hoppefully it works
            if fnmatch.fnmatch(infile, "*.c")==True:
                f.write("\t$(ASTYLE) $(ASTYLE_FLAGS) "+infile+"\n")
                f.write("\t$(CC) -c $(ALL_CFLAGS) -o "+ofile+" "+infile+"\n")
            elif fnmatch.fnmatch(infile, "*.cpp")==True:
                f.write("\t$(CPP) -c $(ALL_CPPFLAGS) -o "+ofile+" "+infile+"\n")
            elif fnmatch.fnmatch(infile, "*.S")==True or fnmatch.fnmatch(infile, "*.s")==True:
                self.app.print_verbose("rule: "+fil)
                f.write("\t$(AS) $(ASFLAGS) -c -o "+ofile+" "+infile)
            else:
                self.app.print_warning("Uknown object? '"+infile+"'")

            f.write("\n\n")

        f.write("\n")


    # extrarules comming from moc_ qrc_ or "used defined" rules
    # TODO: this can be improved alot :)
    def add_extra_rules(self, f):
        if len(self.extra_rules)==0:
            return False

        f.write("\n\n\n# Extra rules comsi comsi comsi :)\n\n")
        for fil in self.extra_rules:
            f.write(fil+"\n")

        f.write("\n\n")
        return True






