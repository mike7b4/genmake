#!/usr/bin/python
# kate: space-indent on; indent-width 4; replace-tabs on

import sys
import os
import logging
class Application:
    def __init__(self, name, version):
        self.genmake_exec = sys.argv[0]
        self.debug = False
        self.verbose = 0
        self.color = False
        self.interactive = False
        self.version = version
        self.die = 0
        self.logger = logging.getLogger(name)
        self.logger.setLevel(logging.DEBUG)
        ch = logging.StreamHandler()
        ch.setFormatter(logging.Formatter("%(asctime)s %(message)s", datefmt="%H:%M:%S"))
        self.logger.addHandler(ch)

    def check_arg(self, arg, nextarg):
        if self.cb != None:
            self.cb(self, arg, nextarg)

    def mustdie(self, err):
        self.die = err
        sys.exit(err)
        return err

    def parse_args(self, cb_next):
        self.cb = cb_next
        progname = os.path.basename(sys.argv[0])
        index = 0
        for arg in sys.argv[1:]:
            index += 1
            try:
                nextarg=sys.argv[index+1]
            except:
                nextarg=None
            if arg=="--debug" or arg=="-D":
                self.debug = True
            elif arg=="--color" and os.name=="posix":
                #Colors not works on windows terminal EPICFAIL
                self.color=True
            elif arg=="--verbose" or arg=="-v":
                self.verbose = 1
            elif arg=="--version":
                print (progname+" v"+self.version)
                self.mustdie(1)
            elif arg=="--interactive" or arg=="-I":
                self.interactive = True
            elif arg=="--help":
                print ("Usage: "+sys.argv[0]+" below [OPTIONS]\n")
                self.check_arg(arg, None)
                print ("    --help\t\t\tTell me about it")
                print ("    --verbose, -v\t\t\tVerbose output")
                print ("    --debug, -D\t\t\tTurn on full debug")
                print ("    --color\t\t\tColoring the terminal")
                print ("    --interactive, -I\t\tAsk if an error occur (to solve some issues with user interaction)")
                print ("    --version\t\t\tPrint "+progname+" Version")
                self.mustdie(1)
            else:
                self.check_arg(arg, nextarg)

    def print_error(self, error):
        if self.color == True:
            self.logger.error ("\033[31mError:"+error+"\033[0m")
        else:
            self.logger.error ("Error: "+error)

    def print_ok(self, error):
        if self.color == True:
            self.logger.info ("\033[32m"+error+"\033[0m")
        else:
            self.logger.info (error)

    def print_debug(self, error):
        if self.debug == True:
            self.logger.debug ("Debug: "+error)

    def print_verbose(self, error):
        if self.verbose == True:
            self.logger.info (error)

    def print_warning(self, error):
        if self.color == True:
            self.logger.warning ("\033[36mWarning: "+error+"\033[0m")
        else:
            self.logger.warning ("Warning: "+error)
