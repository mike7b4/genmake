#!/usr/bin/python
# kate: space-indent on; indent-width 4; replace-tabs on

import sys
import os
import httplib
import json

class Toolchain:
    def __init__(self, main, name):
        self.t={}
        self.main = main
        self.t["targetname"] = name
        self.t["hostos"] = sys.platform
        if sys.platform >= "linux":
            #get rid of 2 in linux2
            self.t["hostos"]="linux"
            self.t["sys_includes"]=""
            self.t["path"]="/usr/bin"
            self.t["prefix"]=""
            self.t["texcompile"]="latex"
            self.t["dvipdf"]="dvipdf"
            self.t["doxygen"]="doxygen"
            self.t["linker"]="ld"
            self.t["cc"]="gcc"
            self.t["valacompile"]="valac"
            self.t["pkgconfig"]="pkg-config"
            self.t["qt-rcc"]="rcc"
            self.t["qt-uic"]="uic-qt4"
            self.t["qt-qmake"]="qmake"
            self.t["qt-moc"]="moc-qt4"
            self.t["cc"]="gcc"
            self.t["cpp"]="gcc"
            self.t["imagecreator"]=""
            self.t["objdump"]="objdump"
            self.t["objcopy"]="objcopy"
            self.t["size"]="size"
            self.t["debugger"]="gdb"
            self.t["git"]="git"
            self.t["svn"]="svn"
            self.t["astyle"]="astyle"
        elif sys.platform == "macos":
            self.t["sys_includes"]=""
            self.t["path"]="/usr/bin"
            self.t["prefix"]=""
            self.t["texcompile"]="latex"
            self.t["dvipdf"]="dvipdf"
            self.t["doxygen"]="doxygen"
            self.t["linker"]="ld"
            self.t["cc"]="gcc"
            self.t["valacompile"]="valac"
            self.t["pkgconfig"]="pkg-config"
            self.t["qt-rcc"]="rcc"
            self.t["qt-uic"]="uic-qt4"
            self.t["qt-qmake"]="qmake"
            self.t["qt-moc"]="moc-qt4"
            self.t["cc"]="gcc"
            self.t["cpp"]="gcc"
            self.t["imagecreator"]=""
            self.t["objdump"]="objdump"
            self.t["objcopy"]="objcopy"
            self.t["size"]="size"
            self.t["debugger"]="gdb"
            self.t["git"]="git"
            self.t["svn"]="svn"
            self.t["astyle"]="astyle"
        else:
            self.t["sys_includes"]="C:/Program Files/Windows Is A Burning Platform/"
            self.t["path"]="C:/Program Files/Windows Is A Burning Platform/"
            self.t["prefix"]=""
            self.t["texcompile"]="latex"
            self.t["dvipdf"]="dvipdf"
            self.t["doxygen"]="doxygen"
            self.t["linker"]="ld"
            self.t["cc"]="gcc"
            self.t["valacompile"]="valac"
            self.t["pkgconfig"]="pkg-config"
            self.t["qt-rcc"]="rcc"
            self.t["qt-uic"]="uic-qt4"
            self.t["qt-qmake"]="qmake"
            self.t["qt-moc"]="moc-qt4"
            self.t["cc"]="gcc"
            self.t["cpp"]="gcc"
            self.t["imagecreator"]=""
            self.t["objdump"]="objdump"
            self.t["objcopy"]="objcopy"
            self.t["size"]="size"
            self.t["debugger"]="gdb"
            self.t["git"]="git"
            self.t["svn"]="svn"
            self.t["astyle"]="astyle"

    def dump_settings(self):
        self.main.print_verbose ("Dump settings:\n")
        for key,value in self.t.iteritems():
            self.main.print_verbose(key+": "+value)

    def request(self):
        ind=self.main.genmakehost.find("/")
        if ind==-1:
            host=self.main.genmakehost
            prefix=""
        else:
            host=self.main.genmakehost[0:ind]
            prefix=self.main.genmakehost[ind:]

        try:
            h=httplib.HTTPConnection(host)
            h.request("GET",prefix+"/genmake/toolchains/get/"+self.t["name"]+"/"+self.t["hostos"])
            self.main.print_verbose(prefix+"/genmake/toolchains/get/"+self.t["name"]+"/"+self.t["hostos"])
            r=h.getresponse()
        except:
            self.main.print_warning("Server error Connection failed")
            if self.main.debug==True:
                raise

            return False

        if r.status==200:
            jsondata=r.read()
            try:
                toolchain, length=json.JSONDecoder().raw_decode(jsondata)
                if len(toolchain)==1 and len(toolchain["toolchain"])>0:
                    self.t=toolchain["toolchain"]
                    self.dump_settings()
                    return True
            except:
                self.main.print_warning("Server error responded with: "+jsondata)
                if self.main.debug==True:
                    raise
                return False
        else:
            self.main.print_warning("Server error got: "+str(r.status))
            return False

        if sys.platform>="linux" or sys.platform=="macos":
            self.main.print_warning("Your running on "+self.t["hostos"]+". Couldn't find settings on server. Fallback to native settings. You should configure '"+self.t["name"]+"' or add '"+self.t["name"]+"' for hostos at http://"+self.main.genmakehost+"/genmake/toolchains")
        else:
            self.main.print_warning("Your running "+sys.platform+" Can guess default settings and cant found on server")

        self.dump_settings()

        return False


if __name__ == '__main__':
    from application import *
    app=Application("0.0.0")
    app.genmakehost="7b4.se"
    app.parse_args(None)
    t=Toolchain(app, "arm-elf-gcc")
    t.request()