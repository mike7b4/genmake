#!/usr/bin/python
# kate: space-indent on; indent-width 4; replace-tabs on
# Only *sensored* is using uppercase in source filenames. we will NOT support it
# it may work it may not but the parser will for sure ignore uppercase extensions
import sys
import os
import json
import fnmatch
import re
from application import *
class ParseFile:
    def __init__(self, app):
        self.inbuf = ""
        self.app = app
        self.exclude_targets=[]
        self.include_targets=[]
        self.command=""
        self.cflags=[]
        self.ldflags=[]
        self.libs=[]
        self.fileinfo={
                "call_qtmoc" : False,
                "call_rcc" : False,
                "c" : False,
                "c++" : False,
                "h" : False,
                "asm" : False
        }
        self.die=0;
        self.curline=0
        #startline of possible JSON segment
        self.start=-1

    def dump(self):
        #FIXME
        #possible can be done better but the python JSON API is not well documented
        #or maybe I am just blind
        gen={}
        gen["cflags"]=self.cflags
        gen["ldflags"]=self.ldflags
        gen["exclude_targets"]=self.exclude_targets
        gen["include_targets"]=self.include_targets
        gen["fileinfo"]=self.fileinfo
        return json.dumps(gen, indent = 2)

    def mustdie(self, error, code):
        self.app.print_error (error)
        self.die=code
        return False

    def check_json(self, data):
        try:
            indata=json.JSONDecoder().raw_decode(data)[0]
            self.app.print_debug("check: "+str(indata))
            try:
                self.exclude_targets = indata["exclude_targets"]
            except:
                self.exclude_targets = ""

            try:
                self.include_targets = indata["include_targets"]
            except:
                self.include_targets = ""

            try:
                self.cflags = indata["cflags"]
            except:
                self.cflags = ""

            try:
                self.ldflags = indata["ldflags"]
            except:
                self.ldflags = ""

            for inc in self.include_targets:
                if self.app.lookup(self.exclude_targets, inc)==True:
                    self.mustdie("Error Dublicate in file '"+self.filename+"', below line: "+str(self.start), -1)
                    self.app.print_verbose("Exclude and then Include SAME target ('"+inc+"')!?")
                    return False

        except:
            raise

        return True

    def push_line(self, line):
        deps=""
        #empty line?
        if line=="":
            return False

        if line.find("#include <Q")!=-1:
            self.app.print_ok("File has QTCore dependies, adding configs\n")
            self.app.pkgconfig_add_dependies(["QtCore"])
        if line.find("#include <QDecl")!=-1:
            self.app.print_ok("File has QML dependies, adding configs\n")
            self.app.pkgconfig_add_dependies(["QtDeclarative"])
        if line[0]=="#" and re.search("Q*Widget", line)!=None:
            self.app.print_ok("File has QTGui dependies, adding configs\n")
            self.app.pkgconfig_add_dependies(["QtGui"])
        if line.find("#include <glib")!=-1 or line.find("#include <gobject")!=-1:
            self.app.print_ok("File has glib/gobject dependies, adding configs\n")
            self.app.pkgconfig_add_dependies(["gobject-2.0","glib-2.0"])
        if line.find("#include <gtk/gtk")!=-1:
            self.app.print_ok("File has gtk dependies, adding configs\n")
            self.app.pkgconfig_add_dependies(["gtk+-2.0"])

        if line.find("Q_OBJECT")!=-1:
            self.app.print_ok("QT Object added MOC compiler rules\n")
            self.fileinfo["call_qtmoc"] = True

        if self.start == -1 and line.find("/*") == -1:
            # not added
            return False

        if line.find("/*") != -1:
            self.start=self.curline

        if self.start!=-1 and line.find("*/") != -1:
            pos=self.inbuf.lower().find("json: genmake")
            if pos == -1:
                #reset no JSON tag
                self.app.print_debug ("Segment ignored '"+self.filename+"', line: "+str(self.start)+" to line: "+str(self.curline))
                self.start = -1
                self.inbuf = ""
                return False

            try:
                #get rid of JSON header by remove json: genmake to lineend
                lines= self.inbuf[pos:].split("{")[1:]
                data=""
                data = "{"+data.join(lines)
                return self.check_json(data)
            except ValueError:
                #something is fucked
                self.mustdie("JSON ValueError '"+self.filename+"', below line: "+str(self.start), -1)
                if self.app.debug == True:
                    self.app.print_debug(self.inbuf)
                    raise

                self.app.print_verbose(self.inbuf)
                self.inbuf = ""
                self.start = -1
                return False

        #add to tempbuf
        self.inbuf += line

    #return 0 on success or an error
    def run(self, fname):
        self.filename = fname = os.path.abspath(fname)
        if os.path.isfile(fname)==False:
            self.app.print_error ("stupidity: '"+fname+"' is not a file!")
            return False

        if fnmatch.fnmatchcase(fname, "*.cpp")==True:
            self.fileinfo["c++"]=True
        elif fnmatch.fnmatchcase(fname, "*.c")==True:
            self.fileinfo["c"]=True
        elif fnmatch.fnmatchcase(fname, "*.s")==True:
            self.fileinfo["asm"]=True
        elif fnmatch.fnmatchcase(fname, "*.h")==True:
            self.fileinfo["h"]=True
        elif fnmatch.fnmatchcase(fname, "*.qrc")==True:
            self.fileinfo["call_rcc"]=True
            #no point reading file just ignore    not an error
            return 0

        self.curline=0
        #yes set to ready if no lines readed
        ready=False
        f=open(fname, "r")
        for line in f.readlines():
            self.curline +=1
            ready=self.push_line(line)
            if ready==True or self.die!=0 or self.curline >= self.app.max_lines:
                break

        if ready==True:
            ready="(FOUND)"
        else:
            ready="(NOT_FOUND)"

        self.app.print_debug("File: '"+self.filename+"' read done after: "+str(self.curline)+" lines "+ready)

        f.close();
        return self.die

def cb_next_arg(app, arg):
    if os.path.isfile(arg)==True:
        app.filename=os.path.abspath(arg)


if __name__=="__main__":
    app=Application()
    app.filename=None
    app.parse_args(cb_next_arg)
    parser=ParseFile(app)
    if app.filename == None:
        app.print_error ("EPICFAIL! Give me a filename for freak sake!")
        sys.exit(-1)
    else:
        err=parser.run(sys.argv[1])
        app.print_verbose (parser.dump())
        sys.exit(err)